<?php
/**
 * Child functions and definitions.
 * Text Domain: 
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development
 * and http://codex.wordpress.org/Child_Themes), you can override certain
 * functions (those wrapped in a function_exists() call) by defining them first
 * in your child theme's functions.php file. The child theme's functions.php
 * file is included before the parent theme's file, so the child theme
 * functions would be used.
 *
 * @author    PoLi Postner & Limpinsel GbR
 * @link      http://www.polimehr.de
 * @version   1.0
 */

function wptuts_scripts_basic()
{
	wp_register_script( 'custom-script', get_stylesheet_directory_uri() . '/js/custom.js' );
	wp_enqueue_script( 'custom-script' );
}
add_action( 'wp_enqueue_scripts', 'wptuts_scripts_basic' );
