<?php
/**
 * Grundeinstellungen für WordPress
 *
 * Zu diesen Einstellungen gehören:
 *
 * * MySQL-Zugangsdaten,
 * * Tabellenpräfix,
 * * Sicherheitsschlüssel
 * * und ABSPATH.
 *
 * Mehr Informationen zur wp-config.php gibt es auf der
 * {@link https://codex.wordpress.org/Editing_wp-config.php wp-config.php editieren}
 * Seite im Codex. Die Zugangsdaten für die MySQL-Datenbank
 * bekommst du von deinem Webhoster.
 *
 * Diese Datei wird zur Erstellung der wp-config.php verwendet.
 * Du musst aber dafür nicht das Installationsskript verwenden.
 * Stattdessen kannst du auch diese Datei als wp-config.php mit
 * deinen Zugangsdaten für die Datenbank abspeichern.
 *
 * @package WordPress
 */

// ** MySQL-Einstellungen ** //
/**   Diese Zugangsdaten bekommst du von deinem Webhoster. **/

/**
 * Ersetze datenbankname_hier_einfuegen mit dem Namen
 * mit dem Namen der Datenbank, die du verwenden möchtest.
 */
define('DB_NAME', getenv('DB_NAME'));

/**
 * Ersetze benutzername_hier_einfuegen
 * mit deinem MySQL-Datenbank-Benutzernamen.
 */
define('DB_USER', getenv('DB_USER'));

/**
 * Ersetze passwort_hier_einfuegen mit deinem MySQL-Passwort.
 */
define('DB_PASSWORD', getenv('DB_PASSWORD'));

/**
 * Ersetze localhost mit der MySQL-Serveradresse.
 */
define('DB_HOST', getenv('DB_HOST'));

/**
 * Der Datenbankzeichensatz, der beim Erstellen der
 * Datenbanktabellen verwendet werden soll
 */
define('DB_CHARSET', 'utf8mb4');

/**
 * Der Collate-Type sollte nicht geändert werden.
 */
define('DB_COLLATE', '');

/**#@+
 * Sicherheitsschlüssel
 *
 * Ändere jeden untenstehenden Platzhaltertext in eine beliebige,
 * möglichst einmalig genutzte Zeichenkette.
 * Auf der Seite {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * kannst du dir alle Schlüssel generieren lassen.
 * Du kannst die Schlüssel jederzeit wieder ändern, alle angemeldeten
 * Benutzer müssen sich danach erneut anmelden.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'l~M/0S<!GNd$b+stv;;7J~F?J<y?gcn.IwHo2ajse]Xwfm|_4)  C}/+_TDf|h;0');
define('SECURE_AUTH_KEY',  'W+S]eq|2nwJ}I4qt;4tcd,sfO0OT+Nh}=v>U*Ej!4ob~]eF$9s+UF=-%YrkY2Yix');
define('LOGGED_IN_KEY',    ':voTGvR{?!J7;Oa>k!XLX6-KMJyDoo%n]-dxw`J+o:`~%&4<QMvn-Sx(cl7Lq6^}');
define('NONCE_KEY',        'D,J!lm0%?Y#n}t(=Wzr:Y+pN8X&kQguz>wz-&,]_;-g]2$y|QS~&WlHrXNO87ofU');
define('AUTH_SALT',        '*Zg8lQ2au~}}=Tw%c^#8u{]O`>!!R*tDMz~L<t!bckB:(jkZ8}E#Sm)^0.x;YV@ ');
define('SECURE_AUTH_SALT', '8d{K@^*r*RPtDqFX~`Z=@W0cilmpA2_o}37 )iI7Z~;IduTqa9!|R6|N*K*~7Frb');
define('LOGGED_IN_SALT',   '*QM}cu4X]yI2[n9bHYzZ1y}NiY@]%-Z+p(*46x+BW^9am9#*.m{Ei*@|+pu.1xJ}');
define('NONCE_SALT',       '$C+gzTJo|&`XwS^hf>8PNzagC|]Qtl`AkEkW)[2a|7cA+LPFz/Y=0ZHjx@!6OD?G');

/**#@-*/

/**
 * WordPress Datenbanktabellen-Präfix
 *
 * Wenn du verschiedene Präfixe benutzt, kannst du innerhalb einer Datenbank
 * verschiedene WordPress-Installationen betreiben.
 * Bitte verwende nur Zahlen, Buchstaben und Unterstriche!
 */
$table_prefix  = 'wp_';

/**
 * Für Entwickler: Der WordPress-Debug-Modus.
 *
 * Setze den Wert auf „true“, um bei der Entwicklung Warnungen und Fehler-Meldungen angezeigt zu bekommen.
 * Plugin- und Theme-Entwicklern wird nachdrücklich empfohlen, WP_DEBUG
 * in ihrer Entwicklungsumgebung zu verwenden.
 *
 * Besuche den Codex, um mehr Informationen über andere Konstanten zu finden,
 * die zum Debuggen genutzt werden können.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Das war’s, Schluss mit dem Bearbeiten! Viel Spaß beim Bloggen. */
/* That's all, stop editing! Happy blogging. */

/** Der absolute Pfad zum WordPress-Verzeichnis. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Definiert WordPress-Variablen und fügt Dateien ein.  */
require_once(ABSPATH . 'wp-settings.php');
